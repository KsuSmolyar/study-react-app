import { createSlice } from "@reduxjs/toolkit";

const initialState = { products: [] };

export const cartSlice = createSlice({
	name: "cart",

	// Начальное состояние хранилища, товаров нет
	initialState,

	//Все доступные методы
	reducers: {
		addProduct(state, action) {
			state.products.push(action.payload);
		},
		removeProduct(state, action) {
			state.products = state.products.filter(
				(product) => product.name !== action.payload.name
			);
		},
		setProducts(state, action) {
			state.products = action.payload;
		},
	},
});

export const { addProduct, removeProduct, setProducts } = cartSlice.actions;
export default cartSlice.reducer;

export const selectorCart = (state) => state.cart.products;
export const selectorCount = (state) => state.cart.products.length;
