import { configureStore } from "@reduxjs/toolkit";
import cartReducer from "./cart-reducer/cart-reducer";
import likeReducer from "./like-reducer/like-reducer";
import typeReducer from "./type-reducer/type-reducer";

export const store = configureStore({
	reducer: {
		cart: cartReducer,
		like: likeReducer,
		type: typeReducer,
	},

	// middleware: [logger, actionCounter],
});
