import { createSlice } from "@reduxjs/toolkit";

const initialState = { selectedType: "128ГБ" };

export const typeSlice = createSlice({
	name: "type",

	// Начальное состояние хранилища, товаров нет
	initialState,

	//Все доступные методы
	reducers: {
		addType(state, action) {
			state.selectedType = action.payload;
		},
	},
});

export const { addType } = typeSlice.actions;
export default typeSlice.reducer;

export const selectorSelectedType = (state) => state.type.selectedType;
