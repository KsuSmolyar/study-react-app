import { createSlice } from "@reduxjs/toolkit";

const initialState = { likeIds: [] };

export const likeSlice = createSlice({
	name: "like",
	//Начальное состояние хранилища - лайков нет
	initialState,
	//Все доступные методы
	reducers: {
		addLikeId(state, action) {
			state.likeIds.push(action.payload);
		},
		removeLikeId(state, action) {
			state.likeIds = state.likeIds.filter((id) => id !== action.payload);
		},
		setLikeIds(state, action) {
			state.likeIds = action.payload;
		},
	},
});

export const { addLikeId, removeLikeId, setLikeIds } = likeSlice.actions;
export default likeSlice.reducer;

export const selectorLike = (state) => state.like.likeIds;
export const selectorLikeCount = (state) => state.like.likeIds.length;
