import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./App.css";
import { Footer } from "./components/footer";
import { Header } from "./components/header";
import { PageProduct } from "./components/pages/page-product/page-product";
import { PageIndex } from "./components/pages/page-index/page-index";

function App() {
	return (
		<BrowserRouter>
			<div className='body-container' id='up'>
				<Header />
				<Routes>
					<Route path='/product' element={<PageProduct />} />;
					<Route path='/' element={<PageIndex />} />
				</Routes>
				<Footer />
			</div>
		</BrowserRouter>
	);
}

export default App;
