import React from "react";
import { Link } from "../link";
import "./breadcrumbs.css";

export const Breadcrumbs = ({ links }) => {
	return (
		<nav className='breadcrumbs'>
			{links.map((link, index) => (
				<div className='breadcrumbs__link' key={link.content}>
					<Link href={link.url}>{link.content}</Link>
					{index < links.length - 1 && <span> {">"} </span>}
				</div>
			))}
		</nav>
	);
};
