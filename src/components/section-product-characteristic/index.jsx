import React from "react";
import { Characteristics } from "../characteristics";

export const SectionProductCharacteristic = () => {
	return (
		<section className='params-section'>
			<h5 className='params-section__title'>Характеристики товара</h5>
			<Characteristics />
		</section>
	);
};
