import React from "react";

export const FeedbackCard = ({
	userPhoto,
	userName,
	rating,
	expirience,
	advantage,
	disadvantage,
}) => {
	const stars = new Array(5).fill();
	return (
		<div className='feedbackCard'>
			<div className='separator'></div>
			<div className='feedback-review__review-card'>
				<img
					className='review-card__userphoto'
					src={userPhoto}
					alt='фото пользователя'
					title='Фотография пользователя'
				/>
				<div className='review-card__content-header'>
					<h4>{userName}</h4>
					<div className='rating-result'>
						{stars.map((_, index) => (
							<img
								className='rating-result__item'
								src={
									index < rating
										? "./resource/images/Звезда.png"
										: "./resource/images/Звезда серая.png"
								}
								alt={index < rating ? "Желтая звезда" : "Серая звезда"}
								key={index}
							/>
						))}
					</div>
				</div>

				<div className='review-card__content-info'>
					<p>
						<b>Опыт использования:</b> {expirience}
					</p>
					<p>
						<b>Достоинства:</b>
						<br />
						{advantage}
					</p>

					<p>
						<b>Недостатки:</b>
						<br />
						{disadvantage}
					</p>
				</div>
			</div>
		</div>
	);
};
