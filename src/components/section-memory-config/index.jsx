import React, { useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { configs } from "../../data/configs";
import {
	addType,
	selectorSelectedType,
} from "../../store/type-reducer/type-reducer";
import { RadioButton } from "../ui/radio-button";

export const SectionMemoryConfig = () => {
	const selectedType = useSelector(selectorSelectedType);
	const dispatch = useDispatch();
	const handleChange = useCallback(
		(e) => {
			const action = addType(e.currentTarget.value);
			dispatch(action);
		},
		[dispatch]
	);

	return (
		<section className='params-section'>
			<h5 className='params-section__title'>Конфигурация памяти: 128 ГБ</h5>
			<div className='options'>
				{/*Радио-кнопки выбора параметров памяти*/}
				{configs.map((config) => (
					<RadioButton
						name='memory-select'
						value={config.value}
						elemClassName=' radio-btn__elem-btn'
						key={config.value}
						onChange={handleChange}
						checked={selectedType === config.value}
					>
						{config.value}
					</RadioButton>
				))}
			</div>
		</section>
	);
};
