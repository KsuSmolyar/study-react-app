import React from "react";
import { Link } from "react-router-dom";
import "./page-index.css";

export const PageIndex = () => {
	return (
		<div className='main-wrapper'>
			<div className='main-content'>
				<p>
					Здесь должно быть содержимое главной страницы. Но в рамках курса
					главная страница используется лишь в демонстрационных целях
				</p>
				<Link to='product' className='link-primary'>
					Перейти на страницу товара
				</Link>
			</div>
		</div>
	);
};
