import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
	addProduct,
	removeProduct,
	selectorCart,
	setProducts,
} from "../../store/cart-reducer/cart-reducer";
import { Button } from "../ui/button";
import { ButtonLike } from "../ui/button-like";
import "./orderInfo.css";

const productData = {
	name: "Смартфон Apple iPhone 13",
	color: "Синий",
	memorySelected: 128,
};

export const OrderInfo = () => {
	const cart = useSelector(selectorCart);
	const dispatch = useDispatch();
	const [text, setText] = useState("Добавить в корзину");
	const [btnThird, setBtnThird] = useState("");

	const handlerClick = (e) => {
		// const cartButton = e.currentTarget;

		const prod = cart.find((prod) => prod.name === productData.name);
		if (prod) {
			const action = removeProduct(prod);
			dispatch(action);
			setText("Добавить в корзину");
			setBtnThird("");
			localStorage.setItem(
				"cart",
				JSON.stringify(cart.filter((prod) => prod.name !== productData.name))
			);
		} else {
			const action = addProduct(productData);
			dispatch(action);
			setText("Товар уже в корзине");
			setBtnThird("btn_third");
			localStorage.setItem("cart", JSON.stringify([...cart, productData]));
		}
	};

	useEffect(() => {
		function restoreCart() {
			const storedCart = localStorage.getItem("cart");
			if (storedCart) {
				const action = setProducts(JSON.parse(storedCart));
				dispatch(action);
				if (
					JSON.parse(storedCart).find((prod) => prod.name === productData.name)
				) {
					setText("Товар уже в корзине");
					setBtnThird("btn_third");
				}
			}
		}
		restoreCart();
	}, []);

	return (
		<div className='order-info'>
			<div className='order-info__price'>
				<div className='order-info__price-value'>
					<div className='price__previous'>
						<div className='price__previous-value'>75 990₽</div>
						<div className='price__previous-discount'>-8%</div>
					</div>

					<div className='price__current'>67 990₽</div>
				</div>
				<ButtonLike />
			</div>
			{/*Блок с информацией по доставке*/}
			<div className='order-info__delivery'>
				<p>
					Самовывоз в четверг, 1 сентября — <b>бесплатно</b>
				</p>
				<p>
					Курьером в четверг, 1 сентября — <b>бесплатно</b>
				</p>
			</div>

			<Button
				id='cartButton'
				buttonType='primary'
				buttonClassName={`btn_cart ${btnThird}`}
				onClick={handlerClick}
			>
				{text}
			</Button>
		</div>
	);
};
