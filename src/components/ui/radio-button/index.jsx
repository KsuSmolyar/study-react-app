import React from "react";
import "./radio-button.css";

export const RadioButton = ({
	name,
	value,
	elemClassName,
	checked,
	onChange,
	children,
}) => {
	return (
		<label className='radio-btn'>
			<input
				className='radio-btn__input visually-hidden'
				type='radio'
				name={name}
				value={value}
				checked={checked}
				onChange={onChange}
			/>
			<div className={"radio-btn__elem " + elemClassName}>{children}</div>
		</label>
	);
};
