import React from "react";

export const ProductImage = ({ src, alt, title }) => {
	return (
		<img
			className='product-image__pic-item'
			src={src}
			alt={alt}
			title={title}
		/>
	);
};
