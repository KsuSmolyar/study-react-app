import React from "react";
import "./button.css";

export const Button = ({
	type = "button",
	buttonType,
	buttonClassName = "",
	id,
	onClick,
	children,
}) => {
	const currentButtonType = `btn_${buttonType}`;
	return (
		<button
			id={id}
			className={`btn ${currentButtonType} ${buttonClassName}`}
			type={type}
			onClick={onClick}
		>
			{children}
		</button>
	);
};
