import React, { useMemo } from "react";
import { reviews } from "../../data/reviews";
import { FeedbackCard } from "../feedback-card";
import "./feedback.css";
import { useSelector } from "react-redux";
import { selectorSelectedType } from "../../store/type-reducer/type-reducer";

export const Feedback = () => {
	const selectedType = useSelector(selectorSelectedType);
	//useMemo использован для запоминания сортированного массива отзывов
	const computedReviewsList = useMemo(() => {
		return [
			...reviews.filter((review) => review.type === selectedType),
			...reviews.filter((review) => review.type !== selectedType),
		];
	}, [selectedType, reviews]);
	return (
		<section className='feedback'>
			<div className='feedback-header'>
				<h3 className='feedback-header__title'>Отзывы</h3>
				<span className='feedback-header__count'>
					<b>425</b>
				</span>
			</div>

			<div className='feedback-review'>
				{computedReviewsList.map((review) => (
					<FeedbackCard
						rating={review.rating}
						userPhoto={review.userPhoto}
						userName={review.userName}
						expirience={review.expirience}
						advantage={review.advantage}
						disadvantage={review.disadvantage}
						key={review.userPhoto}
					/>
				))}
			</div>
		</section>
	);
};
