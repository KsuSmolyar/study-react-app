import React from "react";
import { ProductContent } from "../product-content";
import { ProductImages } from "../product-images";
import styled from "styled-components";

const MainStyled = styled.main`
	margin-bottom: 50px;
`;

export const Main = () => {
	return (
		<MainStyled>
			{/*Контейнер с изображением товара*/}

			<ProductImages />

			{/*Контейнер с характеристиками товара и сайдбаром*/}

			<ProductContent />
		</MainStyled>
	);
};
