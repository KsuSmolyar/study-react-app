import React from "react";
import { ProductImage } from "../ui/product-image";
import "./product-image.css";
import { productImages } from "../../data/product-images.js";

export const ProductImages = () => {
	return (
		<div className='product-image'>
			<h2 className='product-image__title'>Смартфон Apple iPhone 13, синий</h2>

			<div className='product-image__pic'>
				{productImages.map((productImage) => (
					<ProductImage
						src={productImage.src}
						alt={productImage.alt}
						title={productImage.title}
						key={productImage.src}
					/>
				))}
			</div>
		</div>
	);
};
