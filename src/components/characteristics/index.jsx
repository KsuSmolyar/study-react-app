import React from "react";
import { characteristics } from "../../data/characteristics";
import { Characteristic } from "./characteristic";
import style from "./characteristics.module.css";

export const Characteristics = () => {
	return (
		<ul className={style["params-section__list"]}>
			{characteristics.map((characteristic) => (
				<Characteristic
					name={characteristic.name}
					value={characteristic.value}
					href={characteristic.href}
					key={characteristic.name}
				/>
			))}
		</ul>
	);
};
