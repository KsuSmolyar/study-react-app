import React from "react";
import { Link } from "../../link";
import "./characteristic.css";

export const Characteristic = ({ name, value, href }) => {
	return (
		<li className='params-section__list-item'>
			<span>
				{name}:{" "}
				<b>
					{href ? (
						<Link href={href} target='_blank' rel='noreferrer'>
							{value}
						</Link>
					) : (
						value
					)}
				</b>
			</span>
		</li>
	);
};
