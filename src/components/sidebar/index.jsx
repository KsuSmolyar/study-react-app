import React from "react";
import { OrderInfo } from "../orderInfo";
import { SidebarFrame } from "../sidebar-frame";
import "./sidebar.css";

export const Sidebar = () => {
	return (
		<aside className='sidebar'>
			{/*Блок с ценой товара*/}
			<OrderInfo />
			{/*Секция с рекламой*/}
			<SidebarFrame />
		</aside>
	);
};
