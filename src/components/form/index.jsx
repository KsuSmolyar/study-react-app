import React, { useEffect } from "react";
import { useRef } from "react";
import { Button } from "../ui/button";
import "./form.css";

const EMPTY_INPUT_USER_NAME_ERROR = "Вы забыли указать имя и фамилию";
const LENGTH_INPUT_USER_NAME_ERROR = "Имя не может быть короче 2-х символов";
const INPUT_ESTIMATE_ERROR = "Оценка должна быть от 1 до 5";
const getInputName = (elem) => elem.getAttribute("name");

function addErrorClass(input) {
	input.classList.add("error");
}

export const Form = () => {
	const inputUserName = useRef(null);
	const inputEstimate = useRef(null);
	const textArea = useRef(null);

	const handlerSubmit = (e) => {
		e.preventDefault();
		const form = e.currentTarget;
		const inputUserNameValue = inputUserName.current.value.trim();
		const inputEstimateValue = inputEstimate.current.value.trim();

		let text = "";
		if (inputUserNameValue === "" || inputUserNameValue.length <= 2) {
			addErrorClass(inputUserName.current);
			text =
				inputUserNameValue === ""
					? EMPTY_INPUT_USER_NAME_ERROR
					: LENGTH_INPUT_USER_NAME_ERROR;
		}

		if (
			text === "" &&
			(inputEstimateValue === "" ||
				Number.isNaN(+inputEstimateValue) ||
				inputEstimateValue > 5 ||
				inputEstimateValue < 1)
		) {
			addErrorClass(inputEstimate.current);
			text = INPUT_ESTIMATE_ERROR;
		}

		if (text !== "") {
			form.querySelector(
				".findings__input.error ~ .findings__error"
			).textContent = text;
		} else {
			localStorage.removeItem(getInputName(inputUserName.current));
			localStorage.removeItem(getInputName(inputEstimate.current));
			localStorage.removeItem(getInputName(textArea.current));
			inputUserName.current.value = "";
			inputEstimate.current.value = "";
			textArea.current.value = "";
			alert(
				"Ваш отзыв был успешно отправлен и будет отображён после модерации"
			);
		}
	};

	const handlerInput = (e) => {
		const input = e.currentTarget;
		localStorage.setItem(getInputName(input), input.value);
	};

	const handlerFocus = (e) => {
		const input = e.currentTarget;
		input.classList.remove("error");
	};

	useEffect(() => {
		if (
			inputUserName.current === null &&
			inputEstimate.current === null &&
			textArea.current === null
		)
			return;

		function getFromLocalStorage(input) {
			input.value = localStorage.getItem(getInputName(input));
		}

		getFromLocalStorage(inputUserName.current);
		getFromLocalStorage(inputEstimate.current);
		getFromLocalStorage(textArea.current);
	}, []);

	return (
		<section className='form'>
			<form className='feedback-form' action='#' onSubmit={handlerSubmit}>
				<p className='feedback-form__title'>Добавить свой отзыв</p>
				<div className='feedback-form__field'>
					<div className='feedback-form__findings'>
						<div className='findings__wrapper'>
							<input
								ref={inputUserName}
								className='findings__username findings__input'
								type='text'
								name='username'
								placeholder='Имя и фамилия'
								onInput={handlerInput}
								onFocus={handlerFocus}
							/>
							<div className='findings__error'></div>
						</div>
						<div className='findings__wrapper'>
							<input
								ref={inputEstimate}
								className='findings__estimate findings__input'
								type='text'
								name='estimate'
								placeholder='Оценка'
								onInput={handlerInput}
								onFocus={handlerFocus}
							/>
							<div className='findings__error'></div>
						</div>
					</div>
					<div className='feedback-form__text'>
						<textarea
							ref={textArea}
							className='text__review findings__input'
							name='review'
							placeholder='Текст отзыва'
							onInput={handlerInput}
						></textarea>
					</div>
					<div className='feedback-form__btn'>
						<Button type='submit' buttonType='secondary'>
							Отправить отзыв
						</Button>
					</div>
				</div>
			</form>
		</section>
	);
};
