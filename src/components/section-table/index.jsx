import React from "react";
import "./section-table.css";

export const SectionTable = () => {
	return (
		<section className='params-section params-section_hidden'>
			<h5 className='params-section__title'>Сравнение моделей</h5>
			{/*Таблица сравнения моделей*/}
			<table className='params-section__table'>
				<thead>
					<tr className='table__head'>
						<th className='table__head-item'>Модель</th>
						<th className='table__head-item'>Вес</th>
						<th className='table__head-item'>Высота</th>
						<th className='table__head-item'>Ширина</th>
						<th className='table__head-item'>Толщина</th>
						<th className='table__head-item'>Чип</th>
						<th className='table__head-item'>Объём памяти</th>
						<th className='table__head-item'>Аккумулятор</th>
					</tr>
				</thead>

				<tbody>
					<tr className='table__row'>
						<td className='table__cell-item'>Iphone11</td>
						<td className='table__cell-item'>194 грамма</td>
						<td className='table__cell-item'>150,9 мм</td>
						<td className='table__cell-item'>75,7 мм</td>
						<td className='table__cell-item table__cell-item_left'>8,3 мм</td>
						<td className='table__cell-item'>A13 Bionic chip</td>
						<td className='table__cell-item table__cell-item_left'>
							до 128 Гб
						</td>
						<td className='table__cell-item table__cell-item_left'>
							До 17 часов
						</td>
					</tr>
					<tr className='table__row'>
						<td className='table__cell-item'>Iphone12</td>
						<td className='table__cell-item'>164 грамма</td>
						<td className='table__cell-item'>146,7 мм</td>
						<td className='table__cell-item'>71,5 мм</td>
						<td className='table__cell-item table__cell-item_left'>7,4 мм</td>
						<td className='table__cell-item'>A14 Bionic chip</td>
						<td className='table__cell-item table__cell-item_left'>
							до 256 Гб
						</td>
						<td className='table__cell-item table__cell-item_left'>
							До 19 часов
						</td>
					</tr>
					<tr className='table__row'>
						<td className='table__cell-item'>Iphone13</td>
						<td className='table__cell-item'>174 грамма</td>
						<td className='table__cell-item'>146,7 мм</td>
						<td className='table__cell-item'>71,5 мм</td>
						<td className='table__cell-item table__cell-item_left'>7,65 мм</td>
						<td className='table__cell-item'>A15 Bionic chip</td>
						<td className='table__cell-item table__cell-item_left'>
							до 512 Гб
						</td>
						<td className='table__cell-item table__cell-item_left'>
							До 19 часов
						</td>
					</tr>
				</tbody>
			</table>
		</section>
	);
};
