import React from "react";
import "./footer-link.css";

export const FooterLink = () => {
	const handlerClick = () => {
		document.getElementById("up").scrollIntoView({ behavior: "smooth" });
	};
	return (
		<button className='link-primary footer-link' onClick={handlerClick}>
			Наверх
		</button>
	);
};
