import React from "react";
import { Link } from "../link";
import "./footer.css";
import { useCurrentDate } from "@kundinos/react-hooks";
import { FooterLink } from "./footer-link";

export const Footer = () => {
	const currentDate = useCurrentDate({ every: "day" });
	const year = currentDate.getFullYear();
	return (
		<footer className='footer'>
			<div className='wrapper footer__wrapper'>
				<div className='footer__info'>
					<div className='footer__copyright'>
						<p className='footer__copyright-text'>
							&#169; ООО «<span className='accent-text'>Мой</span>Маркет», 2018-
							{year}
						</p>
						<div className='footer-copiright-paragraph'>
							<span>
								Для уточнения информации звоните по номеру{" "}
								<Link href='tel:79000000000'>+7 900 000 0000</Link>,
							</span>
							<span>
								а предложения по сотрудничеству отправляйте на почту{" "}
								<Link href='mailto:partner@mymarket.com'>
									partner@mymarket.com
								</Link>
							</span>
						</div>
					</div>
					<div className='footer__link'>
						<FooterLink />
					</div>
				</div>
			</div>
		</footer>
	);
};
