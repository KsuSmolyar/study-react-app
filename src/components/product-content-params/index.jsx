import React from "react";
import { SectionColor } from "../section-color";
import { SectionMemoryConfig } from "../section-memory-config";
import { SectionProductCharacteristic } from "../section-product-characteristic";
import { SectionProductDescription } from "../section-product-description";
import { SectionTable } from "../section-table";
import "./product-contente-params.css";

export const ProductContentParams = () => {
	return (
		<div className='product-content__params'>
			{/*Секция с цветом*/}
			<SectionColor />
			{/*Секция с конфигурациями памяти товара*/}
			<SectionMemoryConfig />
			{/*Секция характеристики товара*/}
			<SectionProductCharacteristic />
			{/*Секция с описанием товара*/}
			<SectionProductDescription />
			{/*Секция с таблицей сравнение моделей*/}
			<SectionTable />
		</div>
	);
};
