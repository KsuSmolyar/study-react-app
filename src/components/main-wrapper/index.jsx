import React from "react";
import { Feedback } from "../feedback";
import { Form } from "../form";
import { Main } from "../main";
import { Breadcrumbs } from "../breadcrumbs";
import { links } from "../../data/links";

export const MainWrapper = () => {
	return (
		<div className='main-wrapper'>
			{/*Меню со ссылками*/}

			<Breadcrumbs links={links} />

			{/*Основное содержимое сайта*/}

			<Main />

			{/*Секция с отзывами*/}

			<Feedback />

			{/* Секция с формой отправки отзыва */}

			<Form />
		</div>
	);
};
