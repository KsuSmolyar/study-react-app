import React from "react";
import "./sidebar-frame.css";

export const SidebarFrame = () => {
	return (
		<section className='sidebar-frame'>
			<p className='frame__name'>Реклама</p>
			<div className='frame__banner'>
				<iframe className='frame__item' src='./banner.html'></iframe>
				<iframe className='frame__item' src='./banner.html'></iframe>
			</div>
		</section>
	);
};
