import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { selectorCount } from "../../store/cart-reducer/cart-reducer";
import { selectorLikeCount } from "../../store/like-reducer/like-reducer";
import "./header.css";

export const Header = () => {
	const count = useSelector(selectorCount);
	const isShowCounter = count > 0 ? "show-counter" : "";
	const likeCount = useSelector(selectorLikeCount);
	const isShowLikeCounter = likeCount > 0 ? "show-counter" : "";

	return (
		<header className='header'>
			<div className='wrapper header__wrapper'>
				<Link to='/'>
					<div className='header__main'>
						<img
							className='header__logo'
							src='./resource/images/apple-touch-icon.png'
							alt='логотип сайта'
						/>
						<h1 className='header__title'>
							<span className='accent-text'>Мой</span>Маркет
						</h1>
					</div>
				</Link>
				<div className='header__icon'>
					<div
						className={`header__heart ${isShowLikeCounter}`}
						data-count={likeCount}
					>
						<svg
							className='header__heart-img'
							viewBox='0 0 44 35'
							fill='#888888'
							xmlns='http://www.w3.org/2000/svg'
						>
							<path
								fillRule='evenodd'
								clipRule='evenodd'
								d='M3.23828 2.95447C7.22778 -0.875449 13.6743 -0.875449 17.6638 2.95447L21.9301 7.05027L26.1966 2.95447C30.1862 -0.875449 36.6326 -0.875449 40.6222 2.95447C44.6116 6.78439 44.6116 12.973 40.6222 16.803L21.9301 34.7472L3.23828 16.803C-0.75122 12.973 -0.75122 6.78439 3.23828 2.95447ZM14.7175 5.78289C12.3552 3.51507 8.54688 3.51507 6.18455 5.78289C3.82224 8.05071 3.82224 11.7067 6.18455 13.9746L21.9301 29.0904L37.6759 13.9746C40.0382 11.7067 40.0382 8.05071 37.6759 5.78289C35.3137 3.51507 31.5053 3.51507 29.143 5.78289L21.9301 12.7072L14.7175 5.78289Z'
							/>
						</svg>
					</div>
					<div className={`header__cart ${isShowCounter}`} data-count={count}>
						<svg
							className='header__cart-img'
							viewBox='0 0 21 20'
							fill='#888888'
							xmlns='http://www.w3.org/2000/svg'
						>
							<path
								fillRule='evenodd'
								clipRule='evenodd'
								d='M0.799683 0.839218H4.30233H5.12213L5.28291 1.61094L5.74024 3.80617H19.3431H20.5235L20.3295 4.92399L19.2993 10.8579L19.16 11.66H18.3129H7.37647L7.80062 13.696H18.3129V15.616H6.98081H6.16101L6.00023 14.8443L3.93985 4.95443L4.92044 4.76617L3.93985 4.95443L3.48252 2.75922H0.799683V0.839218ZM6.14024 5.72617L6.97647 9.74003H17.4658L18.1626 5.72617H6.14024ZM9.6758 18.0053C9.6758 18.8889 8.9316 19.6034 8.01119 19.6034C7.09075 19.6034 6.34656 18.8889 6.34656 18.0053C6.34656 17.1218 7.09075 16.426 8.01119 16.426C8.9316 16.426 9.6758 17.1218 9.6758 18.0053ZM16.2534 19.6034C17.1738 19.6034 17.918 18.8889 17.918 18.0053C17.918 17.1218 17.1738 16.426 16.2534 16.426C15.3329 16.426 14.5887 17.1218 14.5887 18.0053C14.5887 18.8889 15.3329 19.6034 16.2534 19.6034Z'
							/>
						</svg>
					</div>
				</div>
			</div>
		</header>
	);
};
