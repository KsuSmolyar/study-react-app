import React, { useCallback, useState } from "react";
import { colors } from "../../data/colors";
import { RadioButton } from "../ui/radio-button";
import "./section-color.css";

export const SectionColor = () => {
	const [colorChecked, setColorChecked] = useState("blue");

	const handleChange = useCallback((e) => {
		setColorChecked(e.currentTarget.value);
	}, []);
	return (
		<section className='params-section'>
			<h5 className='params-section__title'>Цвет товара: Синий</h5>

			<div className='options'>
				{/*Радио-кнопки выбора цвета товара*/}
				{colors.map((color) => (
					<RadioButton
						name='color-select'
						value={color.value}
						elemClassName='radio-btn__elem-img'
						key={color.value}
						onChange={handleChange}
						checked={colorChecked === color.value}
					>
						<img
							className='radio-button__img'
							src={color.src}
							alt={color.alt}
							title={color.title}
						/>
					</RadioButton>
				))}
			</div>
		</section>
	);
};
