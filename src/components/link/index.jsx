import React from "react";
import "./link.css";

export const Link = ({ url, target, rel, children }) => {
	return (
		<a className='link-primary' href={url} target={target} rel={rel}>
			{children}
		</a>
	);
};
