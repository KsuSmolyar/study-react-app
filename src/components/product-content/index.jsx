import React from "react";
import { ProductContentParams } from "../product-content-params";
import { Sidebar } from "../sidebar";
import "./product-content.css";

export const ProductContent = () => {
	return (
		<div className='product-content'>
			{/*Контейнер с характиристиками товара*/}

			<ProductContentParams />

			{/*Сайдбар с ценой, кнопкой и рекламой*/}

			<Sidebar />
		</div>
	);
};
