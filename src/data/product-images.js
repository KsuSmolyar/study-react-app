export const productImages = [
	{
		src: "./resource/images/image-1.webp",
		alt: "iPhone13 цвет: синий, вид экрана и задней панели",
		title: "Вид экрана и задней панели",
	},
	{
		src: "./resource/images/image-2.webp",
		alt: "iPhone13 цвет: синий, вид экрана",
		title: "Вид экрана",
	},
	{
		src: "./resource/images/image-3.webp",
		alt: "iPhone13 цвет: синий, вид экрана и задней панели сбоку",
		title: "Вид экрана и задней панели сбоку",
	},
	{
		src: "./resource/images/image-4.webp",
		alt: "iPhone13 цвет: синий, камера",
		title: "Вид камеры",
	},
	{
		src: "./resource/images/image-5.webp",
		alt: "iPhone13 цвет: синий, вид задней панели",
		title: "Вид задней панели",
	},
];
