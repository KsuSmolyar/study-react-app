export const characteristics = [
	{ name: "Экран", value: "6.1" },
	{ name: "Встроенная память", value: "128 ГБ" },
	{ name: "Операционная система", value: "iOS 15" },
	{ name: "Беспроводные интерфейсы", value: "NFC, Bluetooth, Wi-Fi" },
	{
		name: "Процессор",
		href: "https://ru.wikipedia.org/wiki/Apple_A15",
		value: "Apple A15 Bionic",
	},
	{ name: "Вес", value: "173 г" },
];
