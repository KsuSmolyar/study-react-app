export const colors = [
	{
		value: "red",
		src: "./resource/images/color-1.webp",
		alt: "iPhone13 цвет: красный",
		title: "Цвет: красный",
	},
	{
		value: "khaki",
		src: "./resource/images/color-2.webp",
		alt: "iPhone13 цвет: хаки",
		title: "Цвет: хаки",
	},
	{
		value: "pink",
		src: "./resource/images/color-3.webp",
		alt: "iPhone13 цвет: розовый",
		title: "Цвет: розовый",
	},
	{
		value: "blue",
		src: "./resource/images/color-4.webp",
		alt: "iPhone13 цвет: синий",
		title: "Цвет: синий",
	},
	{
		value: "white",
		src: "./resource/images/color-5.webp",
		alt: "iPhone13 цвет: белый",
		title: "Цвет: белый",
	},
	{
		value: "black",
		src: "./resource/images/color-6.webp",
		alt: "iPhone13 цвет: черный",
		title: "Цвет: черный",
	},
];
